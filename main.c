/* 
 * s3dinfo: print details about a given S3D model.
 * You may view the S3D spec here:
 * https://motvl3333.neocities.org/extras/specs/s3d.html
 */

#include <stdio.h>
#include <stdlib.h>

const int version_major = 0;
const int version_minor = 1;

main(argc, argv)
int argc;
char **argv;
{
	int i, buf = 0;
	FILE *p_s3d;

	if(argc < 2)
	{
		display_help();
		return 1;
	}

	p_s3d = fopen(argv[1], "rb");
	if(!p_s3d)
	{
		fprintf(stderr, "Unable to open \"%s\"!\n", argv[1]);
		return 1;
	}

	/* start reading */
	fread(&buf, 4, 1, p_s3d);
	printf("S3D Version:\t0x%.2x\n", buf);

	/* decide how to read data based on the S3D version given */
	switch(buf)
	{
		case 0x00:
		{
			int nvert = 0, npoly = 0;
			int ntris = 0, nquads = 0, unknown = 0;
			int primitive = 0;

			/* skip unused block in HEADER section */
			fseek(p_s3d, 4, SEEK_CUR);

			buf = 0;
			fread(&nvert, 2, 1, p_s3d);
			printf("Vertices:\t%d\n", nvert);

			buf = 0;
			fread(&npoly, 2, 1, p_s3d);
			printf("Polycount:\t%d\n", npoly);

			/* skip VERTEX section */
			fseek(p_s3d, 6*nvert, SEEK_CUR);

			/* count tris and quads */
			for(i = 0; i < npoly; i++)
			{
				primitive = 0;

				fread(&primitive, 4, 1, p_s3d);
				switch(primitive)
				{
					case 0: ntris++;
					break;

					case 1: nquads++;
					break;

					default: unknown++;
					break;
				}

				fseek(p_s3d, 12, SEEK_CUR);
			}

			putchar('\n');
			printf("Tris:\t\t%d\n", ntris);
			printf("Quads:\t\t%d\n", nquads);
			printf("Unknown:\t%d\n", unknown);
		}
		break;

		default:
		{
			fprintf(stderr, "Error: Unknown S3D version or corrupt S3D file!\n");
		}
		return 1;
	}


	/* clean-up */
	fclose(p_s3d);

	return 0;
}

display_help()
{
	printf("Usage: s3dinfo [FILE]\n");
	printf("Version %d.%.3d\n\n", version_major, version_minor);
	printf("Exit codes:\n");
	printf("  0 - OK\n");
	printf("  1 - Minor issue\n");
	printf("  2 - File corrupted\n");
}

